
import { FETCH_RESTUARANT_PLACE } from '../actions'
import { act } from 'react-test-renderer';

const initState = {

    location: []

}

const restaurant = (state = initState, action) => {
  
  
  switch (action.type) {
    case FETCH_RESTUARANT_PLACE:
      return { 
        ...state,
        location: action.payload 
      }
    default:

      return { ...state }
  }
};

export default restaurant

