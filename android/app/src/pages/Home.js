import React from 'react'
import { View, Text ,TouchableOpacity } from 'react-native'
import styles from '../styles/styles'
function Home(props) {
    const { navigation } = props

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Home Screen</Text>
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => navigation.navigate('Restaurant')}>
        <Text style={styles.buttonText}>Go to Restaurant Map</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => navigation.navigate('Test')}>
        <Text style={styles.buttonText}>Go to Test Page</Text>
      </TouchableOpacity>
    </View>
  )
}



export default Home