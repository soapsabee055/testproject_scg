import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Home from '../pages/Home'
import Restaurant from '../pages/Restuarant'
import Test from '../pages/Test'

const Tab = createBottomTabNavigator();


const HomeStack = createStackNavigator();
const TestStack = createStackNavigator()
const RestaurantStack = createStackNavigator()
function HomeStackScreen() {
    return (
        <HomeStack.Navigator>
            <HomeStack.Screen name='Home' component={Home} options={{ title: 'Home' }} />
        </HomeStack.Navigator>
    );
}

function TestStackScreen() {
    return (
        <TestStack.Navigator>
            <TestStack.Screen
                name='Test'
                component={Test}
                options={{ title: 'Test Page' }}
            />

        </TestStack.Navigator>
    )
}


function RestaurantStackScreen() {
    return (
        <RestaurantStack.Navigator>
            <RestaurantStack.Screen
                name='Restaurant'
                component={Restaurant}
                options={{ title: 'Restaurant Map' }}
            />

        </RestaurantStack.Navigator>
    )
}


function MainStackNavigator() {
    return (
        <NavigationContainer>
            <Tab.Navigator>
                <Tab.Screen name="Home" component={HomeStackScreen} />
                <Tab.Screen name="Test" component={TestStackScreen} options={{ title: 'Test Page' }} />
                <Tab.Screen name="Restaurant" component={RestaurantStackScreen} options={{ title: 'Restaurant Page' }} />

            </Tab.Navigator>

        </NavigationContainer>
    )
}

export default MainStackNavigator