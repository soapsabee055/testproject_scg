/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';

import MainStackNavigator from './android/app/src/navigation/MainStackNavigator'

export default function App() {
  return <MainStackNavigator />
}
